(function () {
    'use strict';

    var defaultTitle = 'My Mobile Watchdog';

    angular
        .module('WatchDogApp')
        .run(run);

    run.$inject = ['$rootScope', '$state', '$stateParams', '$log', 'SpinnerService', '$timeout', 'userService', 'session'];

    function run($rootScope, $state, $stateParams, $log, SpinnerService, $timeout, userService, session) {
        var stateChange = {},
            pageTitleListener;

        $rootScope.pageTitle = defaultTitle;
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        stateChange.unauthorized = $rootScope.$on('unauthorized', function () {
            if (!session.loggedIn) {
                $state.go('login');
            } else {
                // $state.go('secure.settings.billing');
                userService
                    .logout()
                    .then(function (response) {
                        // Need to invalidate all session variables
                        session.isActiveSubscriber = session.loggedIn = false;
                        session.deviceId = undefined;

                        $state.go('login');
                    });
            }

            /* $timeout(function () {
                $state.go('login');
            }); */
        });

        stateChange.start = $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                var hideLogout,
                    paidUser = true;

                if (toState.hasOwnProperty('data')) {
                    if (toState.data.hasOwnProperty('hideLogout')) {
                        hideLogout = toState.data.hideLogout;
                    }

                    if (toState.data.hasOwnProperty('paid')) {
                        paidUser = toState.data.paid;
                    }
                }

                SpinnerService.show();

                if (!session.loggedIn && (angular.isUndefined(hideLogout) || !hideLogout)) {
                    event.preventDefault();
                    $state.go('login', undefined, {reload: true});

                    return;
                }

                // Note: This is where we check if a user needs to see the billing page
                if (session.loggedIn && paidUser) {
                    if (toState.name !== 'secure.settings.billing' && !session.isActiveSubscriber) {
                        event.preventDefault();

                        $state.go('secure.settings.billing', undefined, {reload: true});
                    }
                }

                $log.debug('$stateChangeStart to ' + toState.to +
                    '- fired when the transition begins. toState,toParams : \n', toState, toParams);
            });

        stateChange.error = $rootScope.$on('$stateChangeError',
            function (event, toState, toParams, fromState, fromParams) {
                SpinnerService.hide();

                $log.error('$stateChangeError - fired when an error occurs during transition.');
                $log.error(arguments);
            });

        stateChange.success = $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                SpinnerService.hide();

                $log.debug('$stateChangeSuccess to ' + toState.name + '- fired once the state transition is complete.');
            });

        pageTitleListener = $rootScope.$on('$viewContentLoading', function (event, viewName) {
            if (angular.isDefined(viewName.view) && viewName.view.data && viewName.view.data.pageTitle) {
                $rootScope.pageTitle = viewName.view.data.pageTitle + ' | ' + defaultTitle;
            } else {
                $rootScope.pageTitle = defaultTitle;
            }
        });
    }
}());
