/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('WebsiteController', SiteCtrl);

    SiteCtrl.$inject = ['session', '$scope', 'InfiniteScroll', 'dataService', '$timeout', '$window'];

    function SiteCtrl(session, $scope, InfiniteScroll, dataService, $timeout, $window) {
        var vm = this;

        vm.pager = new InfiniteScroll('websites', {device_id: session.deviceId});
        vm.selectSite = selectSite;
        vm.addSite = addSite;
        vm.saveSite = saveSite;
        vm.closeSite = closeSite;
        vm.template = 'app/components/websites/choose.html';

        activate();

        function activate() {
            vm.container = angular.element('#website-container');
            vm.detailContainer = angular.element('#detail-scroller');

            $scope.$on('removePagerItem', function (event, rowData) {
                var idx = vm.pager.results.indexOf(rowData);

                vm.pager.results.splice(idx, 1);
            });
        }

        function selectSite(site) {
            vm.site = undefined;

            $timeout(function () {
                vm.template = 'app/components/websites/site-details.html';

                vm.details = new InfiniteScroll('websites', {device_id: session.deviceId, id: site.id});

                vm.site = site;
            });
        }

        function addSite() {
            vm.site = {
                device_id: session.deviceId
            };

            vm.template = 'app/components/websites/site-add.html';
        }

        function saveSite() {
            var temp = vm.site.domain;

            // Remove the http(s):// from the domain
            vm.site.domain.replace(/^[\/]+\/\//, '');

            dataService.website
                .create(vm.site)
                .then(function () {
                    $scope.$state.go('secure.websites', null, {reload: true});
                })
                .catch(function (err) {
                    $window.alert('There was a problem saving');
                });
        }

        function closeSite() {
            vm.template = 'app/components/websites/choose.html';
            vm.site = undefined;
        }
    }
}());
