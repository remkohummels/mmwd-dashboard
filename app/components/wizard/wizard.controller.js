/* jshint camelcase: false */
/* jshint maxlen: 300 */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('WizardController', WizardController);

    WizardController.$inject = ['$scope', 'appConfig', '$http', 'dataService', 'growl', 'session', 'SpinnerService', 'ngDialog'];

    function WizardController($scope, appConfig, $http, dataService, growl, session, SpinnerService, ngDialog) {
        var vm = this, dialog = null;

        vm.add = add;
        vm.iCloud = iCloud;
        vm.selectDevice = selectDevice;

        if ($scope.$state.is('secure.wizard.ios')) {
            dialog = ngDialog.open({
                template: 'app/components/wizard/wizard-ios-message.html',
                scope: $scope
            });

            if (angular.isDefined(session.login_user_email)) {
                vm.email = session.login_user_email;
            }
            $http({
                method: 'POST',
                url: appConfig.apiUrl + '/ios_two_factor_mail',
                data: {email:vm.email},
                headers: {'Content-Type': 'application/json'}
            });
        }

        function add(device) {
            if ($scope.deviceForm.$dirty && $scope.deviceForm.$valid) {
                SpinnerService.show();

                dataService
                    .device
                    .add(true, {phone_number: vm.device.phone, display_name: vm.device.displayName})
                    .then(function (response) {
                        growl.success('Your device has been added', {title: 'Device', ttl: 10000});

                        session.deviceId = response.content.id;

                        $scope.$emit('deviceChange');

                        $scope.$state.go('secure.dashboard', undefined, {reload: true});
                    })
                    .catch(function (err) {
                        var message = 'Unable to add device';

                        if (err.data && err.data.error) {
                            message = err.data.error;
                        }

                        growl.error(message, {title: 'Error', ttl: 15000});
                    })
                    .finally(SpinnerService.hide);
            } else {
                growl.warn('Please validate the data entered.', {title: 'Invalid Device Information', ttl: 10000});
            }
        }

        function iCloud() {
            if ($scope.deviceForm.$dirty && $scope.deviceForm.$valid) {
                SpinnerService.show();

                dataService
                    .icloud(vm.icloud.username, vm.icloud.password)
                    .then(function (response) {
                        vm.devices = response.devices;
                        $scope.$state.go('secure.wizard.ios.devices');
                    })
                    .catch(function (err) {
                        if (err.status === 422) {
                            var growlTitle = 'Invalid Device Information';
                            if (err.data.code === 97) {
                                growlTitle = 'Configure iCloud';
                            }
                            growl.warning(err.data.message, {
                                title: growlTitle,
                                ttl: 10000
                            });
                        } else {
                            growl.warning('Please try again.', {
                                title: 'Server Exception',
                                ttl: 10000
                            });
                        }
                    })
                    .finally(SpinnerService.hide);
            } else {
                growl.warning('Please validate the data entered.', {title: 'Invalid Device Information', ttl: 10000});
            }
        }

        function selectDevice(deviceId) {
            var device = vm.devices[deviceId];

            SpinnerService.show();

            return dataService
                .device
                .add(false,
                    {
                        phone_number: vm.device.phone,
                        display_name: vm.device.displayName,
                        uuid: deviceId,
                        friendly_name: vm.device.displayName,
                        icu: vm.icloud.username,
                        icp: vm.icloud.password,
                        device_key: deviceId,
                        ios_version: device.ios_version,
                        device_name: device.device_name,
                        device_model: device.model,
                        device_serial: device.serial,
                        phone_name: device.name,
                        latest_backup: device['latest-backup']
                    })
                .then(function (response) {
                    growl.success('Your iOS device has been added', {title: 'Device', ttl: 10000});

                    session.deviceId = response.content.id;

                    $scope.$state.go('secure.dashboard');
                })
                .catch(function (err) {
                    growl.warning('Unable to save your device.', {title: 'Problem Adding Device', ttl: 10000});
                })
                .finally(SpinnerService.hide);
        }
    }
}());
