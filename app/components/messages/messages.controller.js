/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('MessageController', MsgCtrl);

    MsgCtrl.$inject = ['session', '$scope', 'InfiniteScroll', 'dataService', 'appConfig', '$filter', 'SpinnerService'];

    function MsgCtrl(session, $scope, InfiniteScroll, dataService, appConfig, $filter, SpinnerService) {
        var vm = this,
            templates = {
                none: 'app/components/messages/message-none.html',
                details: 'app/components/messages/message-details.html'
            };

        vm.pager = new InfiniteScroll('messages', {device_id: session.deviceId});
        vm.selectRecipient = selectRecipient;
        vm.template = templates.none;
        vm.closeMessage = closeMessage;
        vm.getAttachmentUrl = getAttachmentUrl;

        activate();

        function activate() {
            vm.container = angular.element('#message-container');

            $scope.$on('removePagerItem', function (event, rowData) {
                var idx = $scope.pager.results.indexOf(rowData);

                $scope.pager.results.splice(idx, 1);
            });
        }

        function selectRecipient(recipient) {
            SpinnerService.show();

            vm.recipient = recipient;

            dataService
                .messages(0, 500, session.deviceId, recipient.log_contact_id)
                .then(function (response) {
                    var filter = $filter('orderBy'),
                        results = [];

                    angular.forEach(response.messages, function (value, key) {
                        results = results.concat(value);
                    });

                    vm.messages = filter(results, 'device_created_on', true);
                    vm.template = templates.details;
                })
                .finally(function () {
                    SpinnerService.hide();
                });
        }

        function getAttachmentUrl(attachment) {
            return appConfig.apiUrl + '/attachments?id=' + attachment.id + '&signature=' + attachment.signature;
        }

        function closeMessage() {
            vm.messages = undefined;
            vm.recipient = undefined;
            vm.template = templates.none;
        }
    }
}());
