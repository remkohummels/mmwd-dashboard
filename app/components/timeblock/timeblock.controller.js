/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('TimeBlockController', TimeBlockCtrl)
        .filter('timeBlockDays', TimeBlockDays)
        .filter('timeBlockRange', TimeBlockRange);

    TimeBlockDays.$inject = ['$window'];

    function TimeBlockDays($window) {
        return function (input) {
            var daysEnabled = [];

            if (input.sun) {
                daysEnabled.push('SUN');
            }
            if (input.mon) {
                daysEnabled.push('MON');
            }
            if (input.tue) {
                daysEnabled.push('TUE');
            }
            if (input.wed) {
                daysEnabled.push('WED');
            }
            if (input.thu) {
                daysEnabled.push('THU');
            }
            if (input.fri) {
                daysEnabled.push('FRI');
            }
            if (input.sat) {
                daysEnabled.push('SAT');
            }
            return daysEnabled.join('/');
        };
    }

    TimeBlockRange.$inject = ['$window', 'moment'];

    function TimeBlockRange($window, moment) {
        var fmtTime = function (fixMe) {
            var time24 = moment(('0000' + fixMe).slice(-4), 'HH:mm');
            return (time24.minute() !== 0) ? time24.format('h:mmA') : time24.format('hA');
        };
        return function (input) {
            return fmtTime(input.start_on) + ' - ' + fmtTime(input.end_on);
        };
    }

    TimeBlockCtrl.$inject = ['session', '$scope', 'InfiniteScroll', 'dataService', 'SpinnerService', 'growl', '_', '$window'];

    function TimeBlockCtrl(session, $scope, InfiniteScroll, dataService, SpinnerService, growl, _, $window) {
        var vm = this,
            templates = {
                none: 'app/components/timeblock/timeblock-none.html',
                details: 'app/components/timeblock/timeblock-detail.html',
                detailView: 'app/components/timeblock/timeblock-detail-view.html'
            },
            timeBlockBlank = {
                timeblockId: '',
                timeBlockName: '',
                isEnabled: false,
                days: {
                    sun: false,
                    mon: true,
                    tue: true,
                    wed: true,
                    thu: true,
                    fri: true,
                    sat: false
                },
                startTime: '',
                endTime: ''
            };

        function toggleDay(day) {
            vm.timeblock.timeblock_json.days[day] = !vm.timeblock.timeblock_json.days[day];
        }

        vm.timeblockSelected = false;
        vm.addTimeBlock = addTimeBlock;
        vm.closeTimeBlock = closeTimeBlock;
        vm.toggleDay = toggleDay;
        vm.saveTimeBlock = saveTimeBlock;
        vm.selectTimeBlock = selectTimeBlock;
        vm.template = templates.none;
        vm.pager = new InfiniteScroll('time_block', {device_id: session.deviceId});

        activate();

        function activate() {
            vm.container = angular.element('#record-container');
            vm.detailView = templates.detailView;
            vm.timeBlockName = 'Add/Edit Time Block';

            var times = [], i, j;
            times.push({id: '30', name: '12:30 AM'});

            for (i = 1; i < 12; i++) {
                times.push({id: i + '00', name: i + ':00 AM'});
                times.push({id: i + '30', name: i + ':30 AM'});
            }

            times.push({id: '1200', name: '12:00 PM'});
            times.push({id: '1230', name: '12:30 PM'});

            for (i = 1, j = 13; i < 12; i++, j++) {
                times.push({id: j + '00', name: i + ':00 PM'});
                times.push({id: j + '30', name: i + ':30 PM'});
            }
            times.push({id: '0', name: '12:00 AM'});

            vm.times = times;

            $scope.$on('removePagerItem', function (event, rowData) {
                var idx = vm.pager.timeblocks.indexOf(rowData);
                vm.pager.timeblocks.splice(idx, 1);
            });
            $scope.$on('updatePagerItem', function (event, rowData) {
                var idx = vm.pager.timeblocks.indexOf(rowData);
                vm.pager.timeblocks.splice(idx, 1);
                vm.pager.timeblocks.splice(idx, 0, rowData);
            });
        }

        function selectTimeBlock(timeblock) {
            SpinnerService.show();

            vm.timeblockSelected = true;
            vm.template = templates.details;
            vm.timeblock = {};
            vm.timeblock.timeblock_json = angular.copy(timeBlockBlank);
            vm.timeblock.timeblock_json.timeBlockName = timeblock.display_name;
            vm.timeblock.timeblock_json.timeblockId = timeblock.id;
            vm.timeblock.timeblock_json.isEnabled = timeblock.is_enabled;
            vm.timeblock.timeblock_json.days.sun = timeblock.sun;
            vm.timeblock.timeblock_json.days.mon = timeblock.mon;
            vm.timeblock.timeblock_json.days.tue = timeblock.tue;
            vm.timeblock.timeblock_json.days.wed = timeblock.wed;
            vm.timeblock.timeblock_json.days.thu = timeblock.thu;
            vm.timeblock.timeblock_json.days.fri = timeblock.fri;
            vm.timeblock.timeblock_json.days.sat = timeblock.sat;
            vm.timeblock.timeblock_json.startTime = {id:timeblock.start_on};
            vm.timeblock.timeblock_json.endTime = {id:timeblock.end_on};

            SpinnerService.hide();
        }

        function closeTimeBlock() {
            vm.timeblock = undefined;
            vm.template = templates.none;
            vm.timeblockSelected = false;
            $window.console.log(vm.timeblock);
        }

        function formIsValid() {
            var daysValid = false, d, start, end, tbName;
            for (d in vm.timeblock.timeblock_json.days) {
                if (vm.timeblock.timeblock_json.days[d]) {
                    daysValid = true;
                }
            }
            tbName = vm.timeblock.timeblock_json.timeBlockName;

            if (tbName.trim() === '') {
                growl.error('Please enter a name for this time block.', {title: 'Time Block', ttl: 10000});
                return false;
            }

            // $window.console.log('daysValid', daysValid);
            if (!daysValid) {
                growl.error('Please select at least one day.', {title: 'Time Block', ttl: 10000});
                return false;
            }

            start = vm.timeblock.timeblock_json.startTime.id;
            end = vm.timeblock.timeblock_json.endTime.id;

            // check for valid start and end times
            if (isNaN(parseFloat(start)) || !isFinite(start)) {
                growl.error('Please choose a start time.', {title: 'Time Block', ttl: 10000});
                return false;
            }
            if (isNaN(parseFloat(end)) || !isFinite(end)) {
                growl.error('Please choose an end time.', {title: 'Time Block', ttl: 10000});        return false;
            }
            if (start === end) {
                growl.error('Start and end times cannot be the same.', {title: 'Time Block', ttl: 10000});
                return false;
            }
            return true;
        }

        function addTimeBlock() {
            var tempTimeBlock = {};
            vm.timeblockSelected = true;

            tempTimeBlock.timeblock_json = angular.copy(timeBlockBlank);
            tempTimeBlock.activity = [];

            vm.timeblock = tempTimeBlock;
            vm.template = templates.details;
            vm.timeblockSelected = true;
        }

        function saveTimeBlock() {
            var timeblock = vm.timeblock,
                cj = _.compactObject(timeblock.timeblock_json);

            if (!formIsValid()) {
                return false;
            }

            if (cj.timeblockId) {
                // Save
                dataService
                    .timeblock
                    .update(
                        session.deviceId,
                        cj.timeblockId,
                        cj)
                    .then(function (response) {
                        growl.success('Time block successfully saved', {title: 'Time Block', ttl: 10000});
                        // refresh the list
                        vm.pager = new InfiniteScroll('time_block', {device_id: session.deviceId});
                        closeTimeBlock();
                    })
                    .catch(function (err) {
                        growl.error('Unable to update time block', {title: 'Time Block', ttl: 15000});
                    });
            } else {
                // Add
                // cj.HomeAddress = _.compactObject(cj.HomeAddress);
                // cj.WorkAddress = _.compactObject(cj.WorkAddress);
                dataService
                    .timeblock
                    .create(
                        session.deviceId,
                        cj.timeBlockName,
                        cj)
                    .then(function (response) {
                        growl.success('Time block successfully added', {title: 'Time Block', ttl: 10000});
                        // refresh the list
                        vm.pager = new InfiniteScroll('time_block', {device_id: session.deviceId});
                        closeTimeBlock();
                    })
                    .catch(function (err) {
                        growl.error('Unable to add time block', {title: 'Time Block', ttl: 15000});
                    });
            }
        }
    }
}());
