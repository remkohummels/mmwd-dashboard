(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('ModalInstanceController', ModalInstanceController);

    angular
        .module('WatchDogApp')
        .controller('SignUpController', SignUpCtrl);

    ModalInstanceController.$inject = ['$scope', '$uibModalInstance', '$window'];

    SignUpCtrl.$inject = ['$scope', 'dataService', 'growl', '$log', 'SpinnerService', 'adConversion', '$window', '$uibModal', '$interval', '$location', '$cookies'];

    function ModalInstanceController($scope, $uibModalInstance, $window) {
        var vm = $scope;
        vm.close = function () {
            $uibModalInstance.close();
        };
    }

    function SignUpCtrl($scope, dataService, growl, $log, SpinnerService, adConversion, $window, $uibModal, $interval, $location, $cookies) {
        var vm = this, modalInstance = $uibModal, timeNoActivity = 0, timeInterval, cookieDate;

        // any mouse action resets the timer
        angular.element(document).on('click, mousedown, mouseup', 'body', function () {
            timeNoActivity = 0;
        });
        // typing (e.g. form entry) resets the timer
        $window.onkeydown = function () {
            timeNoActivity = 0;
        };

        function updateTimeNoActivity() {
            // temporarily disabled this feature by using +=0 instead of +=1
            timeNoActivity += 0;

            if (timeNoActivity > 15) {
                timeNoActivity = -10000;
                // only show modal on sign up and billing forms
                openModal();
            }
        }

        timeInterval = $interval(updateTimeNoActivity, 1000);

        function openModal() {
            if (angular.element('#new-account-information').is(':visible') || angular.element('#billing-form').is(':visible')) {
                // do not show the popup again for this page load
                $interval.cancel(timeInterval);

                modalInstance.open({
                    animation: true,
                    templateUrl: 'app/components/login/modal.html',
                    controller: 'ModalInstanceController',
                    openedClass: 'iframe-modal',
                    size: 'lg',
                    resolve: {
                        // resolution
                    }
                });
            }
        }

        // open the modal when user clicks the "<" back arrow
        angular.element('.backBtn').click(function () {
            openModal();
        });

        vm.user = {};
        vm.passwordsMatch = passwordsMatch;
        vm.signUp = signUp;

        // vm.user.promo = $location.search().promo;
        if ($location.search().promo === 'protect-your-kids') {
            vm.user.promo = '7DAY';

            // cookie expires in 1 year
            cookieDate = new Date();
            cookieDate.setFullYear(cookieDate.getFullYear() + 1);
            $cookies.put('protect-your-kids', '1', {expires:cookieDate.toGMTString() + ';'});
        } else if (angular.isDefined($location.search().promocode)) {
            vm.user.promo = $location.search().promocode;
        }

        activate();

        function activate() {
            if ($scope.$state.is('validate')) {
                dataService.account.validate($scope.$stateParams.id)
                    .then(function (response) {
                        if (response.success) {
                            growl.success('You\'re account has been successfully activated',
                                {
                                    title: 'Account Successfully Validated',
                                    ttl: 10000
                                });
                        } else {
                            growl.warn(response.message, {title: 'Validation Problem', ttl: 10000});
                        }
                    })
                    .catch(function (err) {
                        if (err.status && err.status !== -1) {
                            growl.error(err.data.message, {title: 'Validation Error', ttl: 10000});
                        } else {
                            growl.error('Invalid activation code', {title: 'Validation Error', ttl: 10000});
                        }
                    })
                    .finally(function () {
                        $scope.$state.go('login');
                    });
            } else if ($scope.$state.is('reset')) {
                dataService.account.reset($scope.$stateParams.id)
                    .then(function (response) {
                        if (response.success) {
                            growl.success('You\'re password has been successfully reset',
                                {
                                    title: 'Password Successfully Reset',
                                    ttl: 10000
                                });
                        } else {
                            growl.warn(response.message, {title: 'Validation Problem', ttl: 10000});
                        }
                    })
                    .finally(function () {
                        $scope.$state.go('login');
                    });
            } else if ($scope.$state.is('signUp')) {
                $window.fbq('track', 'AddToCart');
                adConversion.track();
            }
        }

        function signUp() {
            var user = vm.user;

            if ($scope.signUpForm.$invalid || !passwordsMatch()) {
                vm.error = 'Please make sure the form is completely filled';

                return;
            }

            SpinnerService.show();

            dataService
                .account
                .create(user.firstName, user.lastName, user.email, user.password, user.password2, user.promo)
                .then(function (response) {
                    $scope.$state.go('signUp-success');
                })
                .catch(function (err) {
                    $log.info(err);
                })
                .finally(SpinnerService.hide);
        }

        function passwordsMatch() {
            return vm.user.password === vm.user.password2;
        }
    }
}());
