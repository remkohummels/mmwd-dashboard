﻿/* jshint camelcase: false */
(function () {
    'use strict';

    angular.module('WatchDogApp').controller('ContactDetailsController', ContactDetailsCtrl);

    ContactDetailsCtrl.$inject = ['dataService', '$stateParams', '$scope', 'session'];

    function ContactDetailsCtrl(dataService, $stateParams, $scope, session) {
        var vm = this;

        vm.deviceId = session.deviceId;
        vm.contactId = $stateParams.contactId;

        if (vm.contactId) {
            dataService.contacts(0, 1, $scope.deviceId, vm.contactId)
                .then(function (response) {
                    vm.contact = response.content;
                    vm.main.pageTitle = $scope.contact.contact_json.firstName +
                        ' ' + $scope.contact.contact_json.lastName;
                });
        }
    }
}());
