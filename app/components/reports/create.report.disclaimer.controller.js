/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('CreateReportsDisclaimerController', CreateReportsDisclaimerController);

    CreateReportsDisclaimerController.$inject = ['dataService', 'SpinnerService', 'session', '$uibModalInstance'];

    function CreateReportsDisclaimerController(dataService, SpinnerService, session, $uibModalInstance) {
        var vm = this;
        vm.clickOK = clickOK;
        vm.clickCancel = clickCancel;

        function clickOK() {
            $uibModalInstance.close(true);
        }

        function clickCancel() {
            $uibModalInstance.dismiss(false);
        }
    }
}());
