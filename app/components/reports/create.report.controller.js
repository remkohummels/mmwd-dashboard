/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('CreateReportsController', CreateReportsController);

    CreateReportsController.$inject = ['dataService', 'SpinnerService', 'session',
        '$uibModal', '$uibModalInstance', '$log', 'logItem'];

    function CreateReportsController(dataService, SpinnerService, session,
        $uibModal, $uibModalInstance, $log, logItem) {
        var vm = this,
            reportUrl = 'https://www.mymobilewatchdog.com/wix/service/report/getReport';

        vm.clickOK = clickOK;
        vm.clickCancel = clickCancel;
        vm.openDatePicker = openDatePicker;
        vm.datePickerStatus = {dateFrom: false, startDate: false};

        // ?fileName=ParentalLog.pdf&dir=DESC&startDate=1449340200000&endDate=1449945000000
        // logId=536563332&
        // &targetUserId=53329743
        // year=2015&secondOffset=19800&hasDst=0&timezone=SLST
        // &date=12%2F14%2F2015

        vm.generateReport = generateReport;
        vm.reportParameters = {sortDir: 'ASC', endDate: new Date()};

        function clickOK() {
            $uibModalInstance.close();
        }

        function clickCancel() {
            $uibModalInstance.dismiss('cancel');
        }

        function generateReport() {
            $log.info(vm.reportParameters);
            $log.info(logItem);
        }

        function openDatePicker(type) {
            vm.datePickerStatus[type] = true;
        }
    }
}());
