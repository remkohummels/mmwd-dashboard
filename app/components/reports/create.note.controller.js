/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('CreateNoteController', CreateNoteController);

    CreateNoteController.$inject = ['dataService', 'SpinnerService', 'session',
        '$uibModalInstance', '$log', 'logItem'];

    function CreateNoteController(dataService, SpinnerService, session,
        $uibModalInstance, $log, logItem) {
        var vm = this;
        vm.clickOK = clickOK;
        vm.clickCancel = clickCancel;
        vm.note = logItem.note;

        function clickOK() {
            saveNote();
        }

        function saveNote() {
            if (vm.note) {
                logItem.note = vm.note;

                SpinnerService.show();
                dataService.report.updateReportItem(logItem)
                    .then(function (result) {
                    })
                    .finally(function () {
                        SpinnerService.hide();
                        dismissPopup();
                    });
            }else {
                dismissPopup();
            }
        }

        function clickCancel() {
            dismissPopup();
        }

        function dismissPopup() {
            $uibModalInstance.dismiss(false);
        }
    }
}());
