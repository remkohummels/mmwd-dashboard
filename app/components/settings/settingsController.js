﻿/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('SettingsController', SettingsCtrl);

    SettingsCtrl.$inject = [];

    function SettingsCtrl() {
        var vm = this;

        vm.tabs = [
            {heading: 'Devices', route: 'secure.settings.devices'},
            {heading: 'Account', route: 'secure.settings.accounts'},
            {heading: 'Billing', route: 'secure.settings.billing'},
            {heading: 'Alerts', route: 'secure.settings.alerts'},
            {heading: 'Emergency Contacts', route: 'secure.settings.emergency'}
        ];
    }
}());
