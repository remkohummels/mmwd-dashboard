﻿/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('BillingController', BillingCtrl);

    BillingCtrl.$inject = ['$scope', 'billingData', 'accountsData', 'session', 'growl',
        'SpinnerService', '_', '$window', '$cookies'];

    function BillingCtrl($scope, billingData, accountsData, session, growl, SpinnerService, _, $window, $cookies) {
        var vm = this;

        vm.firstName = '';
        vm.lastName = '';
        vm.streetAddress = '';
        vm.oneTimePayment = false;
        vm.isActive = session.isActiveSubscriber;
        vm.isBillingUpdate = false;
        vm.promoCode = false;
        vm.subscribe = subscribe;
        activate();

        function activate() {
            if (!session.isActiveSubscriber) {
                vm.subscriberLink = 'Activate Subscription';
            } else {
                vm.subscriberLink = 'Deactivate Subscription';
            }

            SpinnerService.show();

            billingData
                .get()
                .then(function (response) {
                    $window.console.log('billingdata response', response);
                    vm.billings = response.billing;
                    // this may be overkill...
                    if (angular.isDefined(vm.billings) && angular.isDefined(vm.billings.source) && angular.isDefined(vm.billings.source.billing_source_id)) {
                        vm.isBillingUpdate = true;
                    } else {
                        vm.isBillingUpdate = false;
                    }
                })
                .finally(SpinnerService.hide);
        }

        function subscribe() {
            var form = vm.subscribeForm,
                zip;

            if (form.$pristine || form.$invalid) {
                return;
            }

            if (vm.zip) {
                // Left pad zip with zeroes
                zip = _.padLeft(vm.zip.toString(), 5, '0');
            }
            SpinnerService.show();
            billingData
                .saveOrUpdate(
                    vm.card,
                    vm.cvc,
                    vm.expMonth,
                    vm.expYear,
                    vm.firstName,
                    vm.lastName,
                    vm.streetAddress,
                    vm.city,
                    vm.state,
                    zip,
                    vm.oneTimePayment,
                    vm.promoCode,
                    vm.billings
                )
                .then(function (response) {
                    session.isActiveSubscriber = true;

                    // pixel tracker here
                    if ($cookies.get('protect-your-kids') === '1') {
                        $window.fbq('track', 'Purchase', {value: '14.99', currency:'USD'});
                    }
                    if (vm.isBillingUpdate) {
                        // $scope.$state.go('secure.settings.billing');
                        $scope.$state.go('secure.dashboard');
                        growl.success('Thank you, your payment information has been updated.', {title: 'Updated', ttl: 10000});
                    } else {
                        // After the user is successfully billed, we send them to wizard
                        $scope.$state.go('secure.wizard.choose');
                        growl.success('Congratulations, you are an active subscriber', {title: 'Activated', ttl: 10000});
                    }
                })
                .catch(function (err) {
                    $window.console.log('billing err', err);

                    if (err.data.error.indexOf(' PUT ') !== -1) {
                        // re-try using POST
                        vm.isBillingUpdate = true;

                        // presence of billings object forces PUT
                        vm.billings = {};
                        vm.billings.source = {};
                        vm.billings.source.billing_source_id = '1';

                        subscribe();
                    } else {
                        growl.error(err.data.error, {title: 'Authorization Problem', ttl: 10000});
                    }
                })
                .finally(SpinnerService.hide);
        }
    }
}());
