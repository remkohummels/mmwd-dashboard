(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('CancelController', CancelController);

    CancelController.$inject = ['$scope', 'SpinnerService', 'accountsData', 'growl', 'session', 'userService'];

    /* @ngInject */
    function CancelController($scope, SpinnerService, accountsData, growl, session, userService) {
        var vm = this;

        vm.cancel = cancel;
        vm.reasons = [
            'Price is too high',
            'Not happy with the product',
            'Other'
        ];

        activate();

        function activate() {
        }

        function cancel() {
            SpinnerService.show();

            accountsData
                .account
                .remove(vm.reason)
                .then(function () {
                    return userService
                        .logout();
                })
                .then(function (response) {
                    session.isActiveSubscriber = session.loggedIn = vm.loggedIn = false;
                    session.deviceId = undefined;

                    $scope.$state.go('login');

                    growl.success('Your account has been canceled', {ttl: 10000});
                })
                .catch(function (err) {
                    growl.error('Unable to cancel the account, please contact customer service', {ttl: 10000});
                })
                .finally(SpinnerService.hide);
        }
    }
})();
