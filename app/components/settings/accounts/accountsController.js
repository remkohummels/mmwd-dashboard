﻿/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('AccountsController', AccountCtrl);

    AccountCtrl.$inject = ['dataService', '$scope', 'SpinnerService', 'growl'];

    function AccountCtrl(dataService, $scope, SpinnerService, growl) {
        var vm = this;

        vm.update = update;
        vm.changePassword = changePassword;

        activate();

        function activate() {
            SpinnerService.show();

            dataService
                .users()
                .then(function (response) {
                    if (response.content && response.content.length >= 1) {
                        vm.account = response.content[0];
                    }
                })
                .finally(SpinnerService.hide);
        }

        function update() {
            var updateUserParams = {};

            SpinnerService.show();

            if (vm.account.new_password) {
                updateUserParams = vm.account;
            } else {
                updateUserParams = {
                    id: vm.account.id,
                    first_name: vm.account.first_name,
                    last_name: vm.account.last_name
                };
            }

            dataService.user.update(updateUserParams)
                .then(function (response) {
                    if (response) {
                        $scope.$state.go('^', {}, {reload: true});
                    }
                })
                .finally(SpinnerService.hide);
        }

        function changePassword() {
            if ($scope.passwordForm.$invalid || $scope.passwordForm.$pristine) {
                growl.warning('Please make sure all fields are filled correctly', {ttl: 5000});
                return;
            }

            SpinnerService.show();

            dataService.user.update(vm.account)
                .then(function (response) {
                    if (response) {
                        $scope.$state.go('^');
                    } else {
                        growl.danger('Unable to change your password', {ttl: 10000});
                    }
                })
                .catch(function (err) {
                    growl.danger('Unable to change your password', {ttl: 10000});
                })
                .finally(SpinnerService.hide);
        }
    }
}());
