﻿/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('EmergencyContactsController', EmergencyContactsCtrl);

    EmergencyContactsCtrl.$inject = ['dataService', '$scope', 'confirmBox', 'growl', 'SpinnerService', '$window', '$state'];

    function EmergencyContactsCtrl(dataService, $scope, confirmBox, growl, SpinnerService, $window, $state) {
        var vm = this;
        vm.delete = deleteEmergencyContact;
        vm.addUpdateEmergencyContact = addUpdateEmergencyContact;

        activate();
        if (angular.isDefined($state.params)) {
            vm.econtact = $state.params.econtact;
        } else {
            vm.econtact = {};
        }

        function activate() {
            SpinnerService.show();
            dataService
                .emergencyContacts()
                .then(function (response) {
                    vm.econtacts = response.content.emergency_addresses;
                })
                .finally(SpinnerService.hide());
        }
        function addUpdateEmergencyContact() {
            SpinnerService.show();
            if (vm.econtact.phone_number === '') {
                growl.warning('Please enter a phone number for this emergency address.', {ttl: 5000});
            } else if (vm.econtact.description === '') {
                growl.warning('Please enter a description for this emergency address.', {ttl: 5000});
            } else {
                if (angular.isDefined(vm.econtact.id)) {
                    dataService
                        .emergencyContact
                        .update(vm.econtact)
                        .then(function (response) {
                            SpinnerService.hide();
                            growl.success('The emergency address has been updated', {title: 'Emergency Address', ttl: 5000});

                            $scope.$state.go('secure.settings.emergency', {}, {reload: true});
                        });
                } else {
                    dataService
                        .emergencyContact
                        .add(vm.econtact)
                        .then(function (response) {
                            SpinnerService.hide();
                            growl.success('The emergency address has been added', {title: 'Emergency Address', ttl: 5000});
                            $scope.$state.go('secure.settings.emergency', {}, {reload: true});
                        });
                }
            }
        }
        function deleteEmergencyContact(id) {
            confirmBox
                .showDelete('Delete Emergency Address',
                    '<p><strong>Are you sure you want to delete this emergency address?</strong></p>')
                .then(function (result) {
                    SpinnerService.show();
                    if (result) {
                        dataService
                            .emergencyContact
                            .remove(id)
                            .then(function (response) {
                                SpinnerService.hide();
                                growl.success('The emergency address has been deleted', {title: 'Delete Emergency Address', ttl: 5000});
                                // This is processed in MainController
                                // $scope.$emit('deviceChange');
                                $scope.$state.go('secure.settings.emergency', {}, {reload: true});
                            });
                    }
                });
        }
    }
}());
