/* jshint camelcase:false */
/* global _:false, componentHandler:false, googleTrackConversion:false, moment:false */
(function () {
    'use strict';

    var appConfig = {
        apiUrl: 'https://api.mymobilewatchdog.com/v1',
        avatarUrl: 'https://api.mymobilewatchdog.com/avatar',
        PLATFORM_ANDROID: 1,
        PLATFORM_IOS: 2,
        ROW_LIMIT: 25
    };

    angular
        .module('WatchDogApp')
        .constant('appConfig', appConfig)
        .constant('_', _)
        .constant('componentHandler', componentHandler)
        .constant('googleTrackConversion', googleTrackConversion)
        .constant('moment', moment);

    // Adds compactObject to lodash
    _.mixin({
        compactObject: function (o) {
            var clone = _.clone(o);

            _.each(clone, function (v, k) {
                if (!v) {
                    delete clone[k];
                }
            });

            return clone;
        }
    });
}());
