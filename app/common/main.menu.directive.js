(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .directive('mainMenu', MainMenu);

    MainMenu.$inject = ['$document'];

    function MainMenu($document) {
        function link(scope, el, attrs, ctrl, transcludeFn) {
            var body = $document.find('#main-menu');

            el.append(transcludeFn());

            el.prependTo(body);

            scope.menu = ctrl;

            scope.$on('$destroy', function () {
                el.remove();
            });
        }

        return {
            restrict: 'EA',
            transclude: true,
            link: link
        };
    }
}());
