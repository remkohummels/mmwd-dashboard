/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .filter('aviUrl', AvUrl);

    AvUrl.$inject = ['appConfig'];

    function AvUrl(appConfig) {
        return function (queryString) {
            var apiUrl = appConfig.apiUrl;

            if (!queryString) {
                return queryString;
            }

            apiUrl = apiUrl.replace('/v1', '');
            queryString = queryString.replace('/api', '');

            return apiUrl + queryString;
        };
    }
}());

