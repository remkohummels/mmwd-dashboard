(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .directive('confirm', confirm);

    confirm.$inject = [];

    /* @ngInject */
    function confirm() {
        var directive = {
            link: link,
            restrict: 'A',
            require: '?ngModel'
        };

        return directive;

        function link(scope, element, attr, ctrl) {
            var confirmValue = '';

            if (!ctrl) {
                return;
            }

            attr.$observe('confirm', function (value) {
                confirmValue = value || '';

                ctrl.$validate();
            });

            ctrl.$validators.confirm = function (modelValue, viewValue) {
                return ctrl.$isEmpty(viewValue) || viewValue === confirmValue;
            };
        }
    }
})();
