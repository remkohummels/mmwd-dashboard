/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .directive('statusIcon', function () {
            function getRestrictionStatus(restrictionStatus) {
                var result = 'allow';
                switch (restrictionStatus) {
                    case '1':
                        result = 'allow';
                        break;
                    case '2':
                        result = 'temp_allow';
                        break;
                    case '3':
                        result = 'block';
                        break;
                    case '4':
                        result = 'disable';
                        break;
                    case '5':
                        result = 'enable';
                        break;
                }
                return result;
            }

            function getAlertStatus(alertStatus) {
                var result = 'allow';
                switch (alertStatus) {
                    case '1':
                        result = 'allow';
                        break;
                    case '2':
                        result = 'alert';
                        break;
                }
                return result;
            }

            function getIconAndFill(status) {
                var icon,
                    fillColor,
                    defaultValue = {
                        icon: 'check_circle',
                        fillColor: 'iconAllowFill'
                    },
                    values = {
                        alert: {
                            icon: 'error',
                            fillColor: 'iconAlertFill'
                        },
                        block: {
                            icon: 'block',
                            fillColor: 'iconBlockFill'
                        },
                        temp_allow: {
                            icon: 'alarm_on',
                            fillColor: 'iconTempAllowFill'
                        },
                        delete: {
                            icon: 'delete',
                            fillColor: 'iconDeleteFill'
                        },
                        enable: {
                            icon: 'check_circle',
                            fillColor: 'iconAllowFill'
                        },
                        disable: {
                            icon: 'highlight_off',
                            fillColor: 'iconDeleteFill'
                        }
                    };

                if (values[status]) {
                    return values[status];
                } else {
                    return defaultValue;
                }
            }

            function assignIcon(alertStatus, restrictionStatus, page) {
                var status;
                switch (page) {
                    case 'applications':
                        status = getRestrictionStatus(restrictionStatus);
                        break;
                    case 'websites':
                        if (restrictionStatus === '3') {
                            status = getRestrictionStatus(restrictionStatus);
                        } else {
                            status = getAlertStatus(alertStatus);
                        }
                        break;
                    case 'contacts':
                        status = getAlertStatus(alertStatus);
                        break;
                    case 'timeblocks':
                        status = getRestrictionStatus(restrictionStatus);
                        break;
                }

                return getIconAndFill(status);
            }

            return {
                restrict: 'A',
                scope: {
                    alertStatus: '@',
                    restrictionStatus: '@',
                    classes: '@',
                    page: '@',
                    status: '@'
                },
                link: function (scope, el, attrs) {
                    var rvStatus, rv;

                    el.addClass('material-icons');

                    function processParams(source) {
                        if (scope.fillColor) {
                            // Clean up
                            el.removeClass(scope.fillColor);
                        }

                        scope.icon = source.icon;
                        scope.fillColor = source.fillColor;

                        el.addClass(source.fillColor);
                    }

                    if (attrs.status) {
                        rvStatus = getIconAndFill(attrs.status);

                        processParams(rvStatus);
                    } else {
                        scope.$watch('restrictionStatus', function (newValue) {
                            var rsStatus = assignIcon(attrs.alertStatus, newValue, attrs.page);

                            processParams(rsStatus);
                        });

                        scope.$watch('alertStatus', function (newValue) {
                            var asStatus = assignIcon(newValue, attrs.restrictionStatus, attrs.page);

                            processParams(asStatus);
                        });

                        rv = assignIcon(attrs.alertStatus, attrs.restrictionStatus, attrs.page);

                        processParams(rv);
                    }
                },
                template: '{{icon}}'
            };
        });
}());
