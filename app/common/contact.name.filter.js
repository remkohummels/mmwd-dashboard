/* jshint camelcase: false */
(function () {
    angular
        .module('WatchDogApp')
        .filter('wdContactName', formatContactName);

    function formatContactName() {
        /**
         * This function will show a firstname and lastname.
         * If the fn, ln arent there, show the from handle
         * If the from handle is an email, let it through.
         * If the from handle is an phone number, format it.
         * From handle will always be 10 digits if it's a phone number.
         */
        return function (input) {
            var fn = input.first_name,
                ln = input.last_name,
                fh = input.from_handle ? input.from_handle : input.phone_number,
                fullName = (fn ? fn : '') + ' ' + (ln ? ln : '');

            if (fullName.length > 1) {
                return fullName.trim();
            }

            if (fh && fh.length > 0) {
                if (fh.match(/^[0-9]{10}$/)) {
                    return '(' + fh.substr(0, 3) + ') ' + fh.substr(3, 3) + '-' + fh.substr(6);
                } else {
                    return fh;
                }
            }

            return 'Unknown';
        };
    }
}());
