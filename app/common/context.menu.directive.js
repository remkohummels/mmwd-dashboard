(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .directive('contextMenu', ContextMenu);

    ContextMenu.$inject = ['$document'];

    function ContextMenu($document) {
        function link(scope, el, attrs, ctrl, transcludeFn) {
            var body = $document.find('body');

            el.append(transcludeFn());

            el.prependTo(body);

            scope.menu = ctrl;

            scope.$on('$destroy', function () {
                el.remove();
            });
        }

        return {
            restrict: 'EA',
            transclude: true,
            link: link
        };
    }
}());
