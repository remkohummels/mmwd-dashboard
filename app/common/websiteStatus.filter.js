/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .filter('websiteStatus', websiteStatus);

    function websiteStatus() {
        /**
         * Websites combines two fields to show status.
         */
        return function (site) {
            if (site.restriction_status_id === 3) {
                return 'block';
            }
            if (site.alert_status_id === 1) {
                return 'allow';
            }
            return 'alert';
        };
    }
}());
