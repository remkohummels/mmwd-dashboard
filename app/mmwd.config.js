﻿/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider'];

    function config($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
        $urlRouterProvider.otherwise(function ($injector) {
            var $state = $injector.get('$state');

            $state.go('secure.dashboard');
        });

        $stateProvider
            .state('secure', {
                abstract: true,
                template: '<ui-view class="layout-fill"></ui-view>',
                controller: 'SecureController',
                controllerAs: 'secure'
            })
            .state('secure.dashboard', {
                url: '/dashboard',
                templateUrl: 'app/components/dashboard/dashboard.html',
                controller: 'DashboardController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'My Mobile Watchdog',
                    paid: false
                }
            })
            .state('secure.config-android', {
                templateUrl: 'app/components/dashboard/config-android.html',
                controller: 'DashboardController',
                controllerAs: 'ctrl',
                data: {
                    paid: false
                }
            })
            .state('secure.config-ios', {
                templateUrl: 'app/components/dashboard/config-ios.html',
                controller: 'DashboardController',
                controllerAs: 'ctrl',
                data: {
                    paid: false
                }
            })
            .state('secure.messages', {
                url: '/messages',
                templateUrl: 'app/components/messages/messages-list.html',
                controller: 'MessageController',
                controllerAs: 'ctrl',
                data: {
                    endpoint: 'messages',
                    pageTitle: 'Messages'
                }
            })
            .state('secure.calls', {
                url: '/calls',
                templateUrl: 'app/components/calls/calls-list.html',
                controller: 'CallController',
                controllerAs: 'ctrl',
                data: {
                    endpoint: 'calls',
                    pageTitle: 'Calls'
                }
            })
            .state('secure.location', {
                url: '/location',
                abstract: true,
                template: '<div ui-view id="location" class="layout-fill"></div>'
            })
            .state('secure.location.last', {
                url: '/last',
                templateUrl: 'app/components/locations/locationsList.html',
                controller: 'LocationController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Last Location'
                },
                resolve: {
                    markers: resolvers().locationLast
                }
            })
            .state('secure.location.all', {
                url: '/all',
                templateUrl: 'app/components/locations/locationsList.html',
                controller: 'LocationController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'All Locations'
                },
                resolve: {
                    markers: resolvers().locationAll
                }
            })
            .state('secure.contacts', {
                url: '/contacts',
                templateUrl: 'app/components/contacts/contacts-list.html',
                controller: 'ContactController',
                controllerAs: 'ctrl',
                data: {
                    endpoint: 'contacts',
                    pageTitle: 'Contacts'
                }
            })
            .state('secure.contactDetails', {
                url: '/contacts/:contactId',
                templateUrl: 'app/components/contacts/contactDetails.html',
                controller: 'ContactDetailsController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Contact Details'
                }
            })
            .state('secure.websites', {
                url: '/websites',
                templateUrl: 'app/components/websites/websitesList.html',
                controller: 'WebsiteController',
                controllerAs: 'ctrl',
                data: {
                    endpoint: 'websites',
                    pageTitle: 'Websites'
                }
            })
            .state('login', {
                url: '/login',
                templateUrl: 'app/components/login/login.html',
                controller: 'LoginController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Log In',
                    hideLogout: true
                }
            })
            .state('signUp', {
                url: '/signup',
                templateUrl: 'app/components/login/signup.html',
                controller: 'SignUpController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Sign Up',
                    hideLogout: true
                }
            })
            .state('signUp-landing', {
                url: '/signup-landing',
                templateUrl: 'app/components/login/signup-landing.html',
                controller: 'SignUpController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Sign-Up',
                    hideLogout: true
                }
            })
            .state('signUp-success', {
                templateUrl: 'app/components/login/signup-success.html',
                data: {
                    pageTitle: 'Sign Up',
                    hideLogout: true
                }
            })
            .state('forgot', {
                url: '/forgot',
                templateUrl: 'app/components/login/forgot.html',
                controller: 'LoginController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Forgot Password',
                    hideLogout: true
                }
            })
            .state('forgot-success', {
                url: '/forgot-success',
                templateUrl: 'app/components/login/forgot-success.html',
                data: {
                    pageTitle: 'Forgot Password',
                    hideLogout: true
                }
            })
            .state('secure.settings', {
                url: '/settings',
                templateUrl: 'app/components/settings/settings.html',
                controller: 'SettingsController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Settings'
                }
            })
            .state('secure.settings.devices', {
                url: '/devices',
                templateUrl: 'app/components/settings/devices/deviceList.html',
                controller: 'DeviceListController',
                controllerAs: 'ctrl',
                data: {
                    paid: false,
                    pageTitle: 'Settings - Devices'
                }
            })
            .state('secure.settings.emergency', {
                url: '/emergency',
                templateUrl: 'app/components/settings/emergency/emergencyContactsList.html',
                controller: 'EmergencyContactsController',
                controllerAs: 'ctrl',
                data: {
                    paid: false,
                    pageTitle: 'Settings - Emergency Contacts'
                }
            })
            .state('secure.emergency', {
                abstract: true,
                templateUrl: 'app/components/settings/emergency/placeholder.html',
                data: {
                    pageTitle: 'Settings - Emergency Contacts'
                }
            })
            .state('secure.emergency.add', {
                templateUrl: 'app/components/settings/emergency/emergencyContactsForm.html',
                controller: 'EmergencyContactsController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Add Emergency Contact'
                }
            })
            .state('secure.emergency.edit', {
                templateUrl: 'app/components/settings/emergency/emergencyContactsForm.html',
                controller: 'EmergencyContactsController',
                controllerAs: 'ctrl',
                params: {
                    econtact: null
                },
                data: {
                    pageTitle: 'Add Emergency Contact'
                }
            })
            .state('secure.settings.accounts', {
                url: '/accounts',
                templateUrl: 'app/components/settings/accounts/accounts.html',
                controller: 'AccountsController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Settings - Accounts',
                    paid: false
                }
            })
            .state('secure.settings.accounts.edit', {
                templateUrl: 'app/components/settings/accounts/account-edit.html',
                controller: 'AccountsController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Settings - Accounts',
                    paid: false
                }
            })
            .state('secure.settings.accounts.password', {
                templateUrl: 'app/components/settings/accounts/account-password.html',
                controller: 'AccountsController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Settings - Accounts',
                    paid: false
                }
            })
            .state('secure.settings.billing', {
                url: '/billing',
                templateUrl: 'app/components/settings/billing/billing.html',
                controller: 'BillingController',
                controllerAs: 'ctrl',
                data: {
                    paid: false,
                    pageTitle: 'Settings - Billing'
                }
            })
            .state('secure.settings.billing.update', {
                templateUrl: 'app/components/settings/billing/card-form.html',
                data: {
                    paid: true,
                    pageTitle: 'Settings - Billing'
                }
            })
            .state('secure.cancel', {
                templateUrl: 'app/components/settings/billing/cancel.html',
                controller: 'CancelController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Cancel Account'
                }
            })
            .state('secure.settings.alerts', {
                url: '/alerts',
                templateUrl: 'app/components/settings/alerts/alerts.html',
                controller: 'AlertsController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Settings - Alerts'
                }
            })
            .state('secure.alerts', {
                abstract: true,
                templateUrl: 'app/components/settings/alerts/placeholder.html',
                data: {
                    pageTitle: 'Add E-mail - Alerts'
                }
            })
            .state('secure.alerts.email', {
                templateUrl: 'app/components/settings/alerts/add-email.html',
                controller: 'AlertsMaintController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Add E-mail - Alerts'
                }
            })
            .state('secure.alerts.sms', {
                templateUrl: 'app/components/settings/alerts/add-sms.html',
                controller: 'AlertsMaintController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Add SMS - Alerts'
                }
            })
            .state('secure.reports', {
                url: '/reports',
                templateUrl: 'app/components/reports/reports-list.html',
                controller: 'ReportsController',
                controllerAs: 'ctrl',
                data: {
                    pageTitle: 'Reports'
                }
            })
            .state('secure.time_block', {
                url: '/time_block',
                templateUrl: 'app/components/timeblock/timeblock-list.html',
                controller: 'TimeBlockController',
                controllerAs: 'ctrl',
                data: {
                    endpoint: 'time_block',
                    pageTitle: 'Time Blocks'
                }
            })
            .state('secure.applications', {
                url: '/applications',
                templateUrl: 'app/components/applications/applicationsList.html',
                controller: 'InfiniteScrollListController',
                controllerAs: 'ctrl',
                data: {
                    endpoint: 'applications',
                    pageTitle: 'Applications'
                }
            })
            .state('validate', {
                url: '/validate/email/{id:[a-z0-9\-]{36}}',
                controller: 'SignUpController',
                controllerAs: 'ctrl',
                data: {
                    hideLogout: true,
                    pageTitle: 'Email Validation'
                }
            })
            .state('reset', {
                url: '/password/reset/{id:[a-z0-9\-]{36}}',
                controller: 'SignUpController',
                controllerAs: 'ctrl',
                data: {
                    hideLogout: true,
                    pageTitle: 'Password Reset'
                }
            })
            .state('deepLink', {
                url: '/dl/{id:[a-z0-9\-]{36}}',
                controller: 'DeepLinkController',
                controllerAs: 'ctrl',
                data: {
                    hideLogout: true,
                    pageTitle: 'Unsubscribe'
                }
            })
            .state('secure.wizard', {
                abstract: true,
                template: '<ui-view class="layout-fill layout-column"></ui-view>',
                data: {
                    pageTitle: 'Add Device Wizard',
                    paid: false
                }
            })
            .state('secure.wizard.choose', {
                url: '/wizard',
                templateUrl: 'app/components/wizard/wizard.html'
            })
            .state('secure.wizard.android', {
                controller: 'WizardController',
                controllerAs: 'ctrl',
                templateUrl: 'app/components/wizard/wizard-android.html'
            })
            .state('secure.wizard.android-success', {
                templateUrl: 'app/components/wizard/wizard-android-success.html'
            })
            .state('secure.wizard.ios', {
                controller: 'WizardController',
                controllerAs: 'ctrl',
                templateUrl: 'app/components/wizard/wizard-ios.html'
            })
            .state('secure.wizard.ios.devices', {
                templateUrl: 'app/components/wizard/wizard-ios-icloud.html'
            })
            .state('secure.wizard.icloud.devices', {
                templateUrl: 'app/components/wizard/wizard-ios-icloud-devices.html'
            });

        $httpProvider.defaults.withCredentials = true;
        // For now we need to rely on the #/ being at the beginning so that partials load correctly
        $locationProvider.html5Mode(false);

        function resolvers() {
            resolveMarkers.$inject = ['locationService', 'session'];

            function resolveMarkers(locationService, session) {
                return locationService.lastLocation(session.deviceId);
            }

            locationAll.$inject = ['locationService', 'session'];

            function locationAll(locationService, session) {
                return locationService.loadAll(session.deviceId);
            }

            devices.$inject = ['dataService'];

            function devices(dataService) {
                return dataService
                    .devices()
                    .then(function (response) {
                        return response.content;
                    });
            }

            dashboard.$inject = ['dataService'];

            function dashboard(dataService) {
                return dataService
                    .dashboard()
                    .then(function (response) {
                        return response.devices;
                    });
            }

            return {
                locationLast: resolveMarkers,
                locationAll: locationAll,
                devices: devices,
                dashboard: dashboard
            };
        }
    }
}());
