(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .service('AvatarService', AvatarService);

    AvatarService.$inject = ['appConfig', '$resource', 'ErrorResponseInterceptor'];

    function AvatarService(appConfig, $resource, ErrorResponseInterceptor) {
        var avatarService = {},
            avatarResource = {},
            interceptor = ErrorResponseInterceptor,
            actions = {
                get: {method: 'GET', withCredentials: true, interceptor: interceptor},
                add: {method: 'POST', withCredentials: true, interceptor: interceptor},
                update: {method: 'PUT', withCredentials: true, interceptor: interceptor},
                query: {method: 'GET', isArray: true, withCredentials: true, interceptor: interceptor},
                remove: {method: 'DELETE', withCredentials: true, interceptor: interceptor}
            };

        avatarService.avatar = {
            get: avatarGet,
            upload: avatarUpload
        };

        activate();

        return avatarService;

        function activate() {
            avatarResource.get = $resource(appConfig.apiUrl + '/avatar');
            avatarResource.upload = $resource(appConfig.apiUrl + '/avatar/upload');
        }

        function avatarGet(deviceId) {
            return avatarResource.get.get({});
        }

        function avatarUpload() {
            return avatarResource.upload.add();
        }
    }
}());
