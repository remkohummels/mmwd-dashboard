(function () {
    'use strict';
    angular.module('WatchDogApp')
        .factory('ErrorResponseInterceptor', ErrorResponseInterceptor);

    ErrorResponseInterceptor.$inject = ['$rootScope', '$q'];

    function ErrorResponseInterceptor($rootScope, $q) {
        return {
            responseError: function (rejection) {
                if (rejection.status === 401) {
                    $rootScope.$broadcast('unauthorized');
                }

                return $q.reject(rejection);
            }
        };
    }
}());
