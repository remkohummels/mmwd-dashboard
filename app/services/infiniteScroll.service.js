/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('InfiniteScroll', InfiniteScrollService);

    InfiniteScrollService.$inject = ['dataServiceSimple', '$document', '$window'];

    function InfiniteScrollService(dataService, $document, $window) {
        var loader = $document.find('#ajaxLoader');

        function InfiniteScroll(endpoint, args) {
            var vm = this;

            vm.endpoint = endpoint;
            vm.args = args;
            vm.page = 1;
            vm.busy = false;
            vm.endOfResults = false;
            vm.rowLimit = 25;
            vm.results = [];
            vm.contact = undefined;
            vm.timeblocks = undefined;
            vm.device = undefined;

            vm.nextPage = nextPage;

            function nextPage() {
                var offset = (vm.page - 1) * vm.rowLimit,
                    limit = vm.page * vm.rowLimit;

                if (vm.busy || vm.endOfResults) {
                    return;
                }
                vm.busy = true;

                // only show spinner on initial load.
                if (vm.page === 1) {
                    loader.addClass('layout-column');
                }
                vm.args.offset = offset;
                vm.args.limit = limit;

                dataService.get(vm.endpoint, vm.args)
                    .then(function (response) {
                        if (response.device && angular.isUndefined(vm.device)) {
                            vm.device = response.device;
                        }
                        if (response.contact && angular.isUndefined(vm.contact)) {
                            vm.contact = response.contact;
                        }
                        if (response.time_blocks && angular.isUndefined(vm.timeblocks)) {
                            vm.timeblocks = response.time_blocks;
                        }
                        vm.results
                            .push
                            .apply(
                                vm.results, response[vm.endpoint] ? response[vm.endpoint] : response.content.activity);
                        vm.page++;
                        vm.busy = false;

                        if (response[vm.endpoint] && response[vm.endpoint].length < vm.rowLimit) {
                            vm.endOfResults = true;
                        } else if (
                            response.content &&
                            response.content.activity &&
                            response.content.activity.length < vm.rowLimit) {
                            vm.endOfResults = true;
                        }

                        loader.removeClass('layout-column');
                    })
                    .catch(function () {
                        loader.removeClass('layout-column');
                    });
            }
        }

        return InfiniteScroll;
    }
}());
