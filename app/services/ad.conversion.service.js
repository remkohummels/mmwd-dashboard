/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('adConversion', conversion);

    conversion.$inject = ['googleTrackConversion', '_'];

    /* @ngInject */
    function conversion(googleTrackConversion, _) {
        var defaults = {
            google_conversion_id: 965413230,
            google_conversion_language: 'en',
            google_conversion_format: '3',
            google_conversion_color: 'fffff',
            google_conversion_label: 'kMCoCMvWkGMQ7pKszAM',
            google_conversion_value: 0,
            google_remarketing_only: false
        };

        return {
            track: track
        };

        /**
         * Tracks a conversion via Google Ad Services, you can overwrite the default parameters or add custom params.
         *
         * @see https://developers.google.com/adwords-remarketing-tag/asynchronous/?hl=en
         * @param conversion parameters to pass to Google track conversion
         */
        function track(conversion) {
            var params = _.defaults(conversion || {}, defaults);

            googleTrackConversion(params);
        }
    }
})();
