/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('timeblocksData', TimeblocksDataFactory);

    TimeblocksDataFactory.$inject = ['dataServiceSimple'];

    function TimeblocksDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'timeblocks';

        dataFactory.timeblocks = timeblocks;
        dataFactory.getTimeBlock = getTimeBlock;
        dataFactory.remove = remove;

        return dataFactory;

        function timeblocks(offset, limit, deviceId) {
            var args = {
                offset: offset,
                limit: limit,
                device_id: deviceId
            };
            return dataService.get(endpoint, args);
        }

        function getTimeBlock(deviceId, applicationId) {
            var args = {
                device_id: deviceId,
                id: applicationId
            };
            return dataService.get(endpoint, args);
        }

        function updateStatus(timeblockId, alertStatusId, restrictionStatusId) {
            var args = {
                id: timeblockId,
                alert_status_id: alertStatusId,
                restriction_status_id: restrictionStatusId
            };
            return dataService.update(endpoint, args);
        }

        function remove(deviceId, timeblockId) {
            var args = {
                device_id: deviceId,
                time_block_id: timeblockId
            };
            return dataService.delete(endpoint, args);
        }
    }
}());
