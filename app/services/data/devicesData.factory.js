/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('devicesData', DevicesDataFactory);

    DevicesDataFactory.$inject = ['dataServiceSimple'];

    function DevicesDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'devices';

        dataFactory.devices = devices;
        dataFactory.device = {
            get: getDevice,
            remove: removeDevice,
            sync: sync,
            resendLInk: resendLink
        };

        dataFactory.resendLink = resendLink;
        dataFactory.sync = sync;

        return dataFactory;

        function devices() {
            return dataService.get(endpoint);
        }

        function getDevice(deviceId) {
            return dataService.get(endpoint, {device_id: deviceId});
        }

        function removeDevice(deviceId) {
            return dataService.delete(endpoint, deviceId);
        }

        function sync(deviceId) {
            return dataService.get('sync_device', {device_id: deviceId});
        }

        function resendLink(deviceId) {
            return dataService.get('resend_device_link', {device_id: deviceId});
        }
    }
}());
