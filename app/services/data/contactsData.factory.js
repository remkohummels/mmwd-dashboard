/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('contactsData', ContactsDataFactory);

    ContactsDataFactory.$inject = ['dataServiceSimple'];

    function ContactsDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'contacts';

        dataFactory.contacts = contacts;
        dataFactory.getContact = getContact;
        dataFactory.updateStatus = updateStatus;
        dataFactory.remove = remove;

        return dataFactory;

        function contacts(offset, limit, deviceId) {
            var args = {
                offset: offset,
                limit: limit,
                device_id: deviceId
            };
            return dataService.get(endpoint, args);
        }

        function getContact(deviceId, applicationId) {
            var args = {
                device_id: deviceId,
                id: applicationId
            };
            return dataService.get(endpoint, args);
        }

        function updateStatus(contactId, alertStatusId, restrictionStatusId) {
            var args = {
                id: contactId,
                alert_status_id: alertStatusId,
                restriction_status_id: restrictionStatusId
            };
            return dataService.update(endpoint, args);
        }

        function remove(contactId) {
            return dataService.delete(endpoint, {id: contactId});
        }
    }
}());
