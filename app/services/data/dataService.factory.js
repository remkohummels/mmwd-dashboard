/* jshint camelcase:false */
/* jshint maxstatements:80 */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('dataServiceSimple', DataService);

    DataService.$inject = ['appConfig', '$resource', 'ErrorResponseInterceptor'];

    function DataService(appConfig, $resource, ErrorResponseInterceptor) {
        var dataService = {},
            resources = {},
            interceptor = ErrorResponseInterceptor,
            actions = {
                get: {method: 'GET', withCredentials: true, interceptor: interceptor},
                add: {method: 'POST', withCredentials: true, interceptor: interceptor},
                update: {method: 'PUT', withCredentials: true, interceptor: interceptor},
                query: {method: 'GET', isArray: true, withCredentials: true, interceptor: interceptor},
                remove: {method: 'DELETE', withCredentials: true, interceptor: interceptor}
            },
            actionsCache = {
                get: {method: 'GET', withCredentials: true, cache: true, interceptor: interceptor},
                add: {method: 'POST', withCredentials: true, interceptor: interceptor},
                update: {method: 'PUT', withCredentials: true, interceptor: interceptor},
                query: {method: 'GET', isArray: true, withCredentials: true, interceptor: interceptor},
                remove: {method: 'DELETE', withCredentials: true, interceptor: interceptor}
            };

        dataService.get = get;
        dataService.add = add;
        dataService.update = update;
        dataService.query = query;
        dataService.delete = remove;

        activate();

        return dataService;

        function activate() {
            resources.accounts = $resource(appConfig.apiUrl + '/accounts', undefined, actions);
            resources.sendlink = $resource(appConfig.apiUrl + '/devices/registration/sendlink', undefined, actions);
            resources.sync = $resource(appConfig.apiUrl + '/sync_device', undefined, actions);
            resources.users = $resource(appConfig.apiUrl + '/users', undefined, actions);
            resources.forgot = $resource(appConfig.apiUrl + '/forgot');
            resources.devices = $resource(appConfig.apiUrl + '/devices', undefined, actionsCache);
            resources.dashboard = $resource(appConfig.apiUrl + '/dashboard', undefined, actions);
            resources.calls = $resource(appConfig.apiUrl + '/calls', undefined, actions);
            resources.contacts = $resource(appConfig.apiUrl + '/contacts', undefined, actions);
            resources.messages = $resource(appConfig.apiUrl + '/messages', undefined, actions);
            resources.websites = $resource(appConfig.apiUrl + '/websites', undefined, actions);
            resources.applications = $resource(appConfig.apiUrl + '/applications', undefined, actions);
            resources.statusAlert = $resource(appConfig.apiUrl + '/alert_statuses', undefined, actions);
            resources.statusRestriction = $resource(appConfig.apiUrl + '/restriction_statuses', undefined, actions);
            resources.locations = $resource(appConfig.apiUrl + '/locations', undefined, actions);
            resources.alertAddresses = $resource(appConfig.apiUrl + '/alert_addresses', undefined, actions);
            resources.billings = $resource(appConfig.apiUrl + '/billings', undefined, actions);
            resources.reports = $resource(appConfig.apiUrl + '/logs', undefined, actions);
            resources.time_block = $resource(appConfig.apiUrl + '/time_block', undefined, actions);
        }

        function get(endpoint, args) {
            return resources[endpoint].get(args).$promise;
        }

        function add(endpoint, args) {
            return resources[endpoint].add(args).$promise;
        }

        function update(endpoint, args) {
            return resources[endpoint].update(args).$promise;
        }

        function query(endpoint, args) {
            return resources[endpoint].query(args).$promise;
        }

        function remove(endpoint, args) {
            return resources[endpoint].remove(args).$promise;
        }
    }
}());
