/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('emergencyContactsData', emergencyContactsDataFactory);

    emergencyContactsDataFactory.$inject = ['dataServiceSimple'];

    function emergencyContactsDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'emergencyContacts';

        dataFactory.emergencyContacts = emergencyContacts;
        dataFactory.remove = remove;

        return dataFactory;

        // no device id??
        function emergencyContacts() {
            var args = {};
            return dataService.get(endpoint, args);
        }

        function remove(id) {
            var args = {
                id: id
            };
            return dataService.delete(endpoint, args);
        }
    }
}());
