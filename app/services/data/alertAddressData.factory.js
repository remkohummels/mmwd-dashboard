/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('alertAddressData', AlertAddressDataFactory);

    AlertAddressDataFactory.$inject = ['$resource', 'appConfig', 'ErrorResponseInterceptor'];

    function AlertAddressDataFactory($resource, appConfig, interceptor) {
        var dataFactory,
            resource,
            actions = {
                get: {method: 'GET', withCredentials: true, interceptor: interceptor},
                add: {method: 'POST', withCredentials: true, interceptor: interceptor},
                update: {method: 'PUT', withCredentials: true, interceptor: interceptor},
                query: {
                    method: 'GET',
                    isArray: true,
                    withCredentials: true,
                    interceptor: interceptor,
                    transformResponse: transformResponse
                },
                remove: {method: 'DELETE', withCredentials: true, interceptor: interceptor}
            };

        resource = $resource(appConfig.apiUrl + '/alert_addresses', undefined, actions);

        dataFactory = {
            list: alertAddresses,
            create: alertAddressCreate,
            update: alertAddressUpdate,
            remove: alertAddressRemove
        };

        return dataFactory;

        function transformResponse(data) {
            var response = angular.fromJson(data);

            // This way we can just update the object and call update
            return response.content.alert_addresses;
        }

        function alertAddresses() {
            return resource.query().$promise;
        }

        function alertAddressUpdate(address) {
            return resource
                .update({
                    id: address.id,
                    email: address.email,
                    is_sms: address.is_sms
                })
                .$promise;
        }

        function alertAddressCreate(address) {
            return resource
                .add({
                    email: address.email,
                    is_sms: address.is_sms
                })
                .$promise;
        }

        function alertAddressRemove(addressId) {
            return resource.remove({id: addressId}).$promise;
        }
    }
}());
