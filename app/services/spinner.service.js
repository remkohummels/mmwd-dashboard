(function () {
    'use strict';
    angular
        .module('WatchDogApp')
        .service('SpinnerService', SpinnerService);

    SpinnerService.$inject = ['$document'];

    function SpinnerService($document) {
        var loader = $document.find('#ajaxLoader');

        return {
            show: show,
            hide: hide
        };

        function show() {
            loader.addClass('layout-column');
        }

        function hide() {
            loader.removeClass('layout-column');
        }
    }
}());
