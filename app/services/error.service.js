(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .service('errorService', ErrorService);

    ErrorService.$inject = ['$log'];

    function ErrorService($log) {
        var service = {};

        service.log = logError;
        service.logHandler = logHandler;

        return service;

        function logError(error) {
            $log.error(error);
        }

        function logHandler(error) {
            $log.error(error);
        }
    }
}());
