<a name="1.0.0"></a>
# [1.0.0](https://github.com/mattlewis92/angular-bluebird-promises/compare/1.0.0...v1.0.0) (2015-12-16)


### Bug Fixes

* **commonjs:** fix requiring the module with webpack + browserify. Closes 23 ([1f0c688](https://github.com/mattlewis92/angular-bluebird-promises/commit/1f0c688))



<a name="1.0.0"></a>
# [1.0.0](https://github.com/mattlewis92/angular-bluebird-promises/compare/0.6.4...v1.0.0) (2015-12-16)

* Stable API
